# Tanguy Mervin

[tmervin0010@icloud.com](mailto:tmervin0010@icloud.com) • 0623062379 • [GitLab](https://gitlab.com/accpnt) • [Blog](https://accpnt.eu/) • Paris

## Ingénieur IoT

A la recherche d'un poste d'ingénieur IoT. J'ai auparavant travaillé en tant qu'ingénieur de développement embarqué puis data scientist / statisticien et je suis très à l'aise avec les outils informatiques et d'analyse de données. 

## Compétences

* Data science : traitement de données (R, Python), data visualisation (plotly, matplotlib), statistiques, théorie des graphes, modélisation et calcul matriciel
* Analyse de données: interfaçage API, web scraping, agrégation et reporting, SQL, Splunk
* Embarqué: C sur différentes cibles (MSP430, NIOS II), spécifications et architecture, interfaçage (I2C, UART, SPI)
* Informatique: Python, R, C/C++, Rust, CI/CD sous GitLab, Docker, Linux

## Expériences

### Data Analyst
#### Freelance
##### Novembre 2021 - présent

* Data Analyst pour Siemens Mobility (juin 2022 - présent)
    * Création de dashboard Splunk pour la visualisation et statistiques de KPI d'exploitation ferroviaire (Python, HTML, CSS, SPL), projet NExTEO
* Architecte IoT pour Beerlink (avril 2022 - juillet 2022)
    * Interfaçage avec capteur de pression, prototypage électronique sur Arduino
* Architecte logiciel embarqué sur NIOS II pour New Imaging Technologies (janvier 2022 - avril 2022)

### Data Scientist 
#### DeepLime
##### Janvier 2020 - Août 2021

* Développement d'une librairie de statistiques spatiales
* Validation des algorithmes et modèles par test unitaire (Python, R, CI/CD)

### Ingénieur systèmes de test
#### Siemens Mobility
##### Avril 2018 - Janvier 2020

* Développement embarqué et réseau sur les simulateurs de test des métros automatisés CBTC (C/C++).
* Portage de carte A15 Men Elektronik vers A21 QorIQ (ARM)

### Ingénieur de support technique
#### Freelance
##### Juin 2017 - Décembre 2017

---

### Ingénieur de développement embarqué
#### Elsys Design
##### Août 2010 - Février 2017

* Architecture et développement embarqué sur NIOS II, MSP430 (Linux, FreeRTOS) et intégration FPGA (VHDL) pour Sagem

---

### Working holiday en Nouvelle-Zélande
##### Août 2009 - Mai 2010

---

### Ingénieur de développement embarqué
#### Orange Business Services
##### Mars 2008 - Août 2009

* Architecture et développement embarqué (C, Linux) pour Orange / France Telecom, projet Connexion TGV

---

## Formations

* EFAB: Master 2 Stratégies économiques, numérique et données (SEND) (2021 - 2022)
* CNAM: Master 1 Mathématiques appliquées et statistiques (2018 - 2021)
* CNAM: Licence générale mention Mathématiques Appliquées (2018 - 2019)
* Ulm Universität, Allemagne: Master 2 Communications Technology (Septembre 2006 - Avril 2007)
* ENSSAT, France: Diplôme d'ingénieur en Electronique et Informatique Industrielle (2004 - 2007)
