![](round_profile.png)

# Tanguy Mervin

Ingénieur en mathématiques appliquées • +33 (0)6 23 06 23 79 • Paris • [tmervin0008@icloud.com](mailto:tmervin0008@icloud.com) • [GitLab](https://gitlab.com/accpnt) • [Blog](https://accpnt.eu/)

```Passionné par la data, avec une forte expérience en informatique et en mathématiques```

## Compétences

* Data science: Analyse et traitement de données, reporting (R, Python)
* Econométrie: Extreme Value Theory, copules, métriques du risque, séries chronologiques (GARCH et variantes)
* Fonctionnel: architecture logicielle, développement, intégration et validation, formation des équipes
* Informatique: Python, R, C/C++
* Mathématiques appliquées: implémentation de travaux de recherche en statistiques spatiales et théorie des graphes, modélisation et calcul matriciel
* Systèmes: UNIX, Git, intégration continue CI/CD

## Expériences

### Ingénieur en mathématiques appliquées (secteur énergétique), DeepLime
#### Janvier 2020 - Août 2021

* Optimisation de fosse: développement d'algorithmes de calcul du pourtour optimal, théorie des graphes (Python)
* Développement d'une librairie de géostatistiques: variographie 3D, estimation (Python)
* Intégration continue, automatisation des tests, validation des algorithmes par tests unitaires (R, Python)
* Refactoring de code pour une procédure d'exploitation minière (Python)
* Sessions de formation et d'animation technique autour des géostatistiques et de la théorie des graphes

### Ingénieur systèmes de test (secteur ferroviaire), Siemens Mobility
#### Avril 2018 - Janvier 2020

* Portage de BSP de la carte A15 vers A21 (QorIQ) de chez MEN Mikroelectronik.
* Développement réseau en C++ sur les simulateurs de test des métros automatisés CBTC.
* Data mining, analyse et traitement de logs de simulation (Splunk, Python)
* Intégration avec les partenaires SNCF et RATP sur le projet NExTEO

### Ingénieur en systèmes d'information, freelance
#### Juin 2017 - Décembre 2017

* Développement Python, support système Linux, administration réseau et base de données SQL pour une création d’entreprise.

### Ingénieur de développement (secteur défense), Elsys Design
#### Août 2010 - Février 2017

* Conception, développement et intégration sur des systèmes modulaires dédiés aux caméras thermiques de Sagem Argenteuil.
* Développement d’un testbench et bootloader pour Millipore

### Working holiday en Nouvelle-Zélande
#### Août 2009 - Mai 2010

* Barista au Café 121, Ponsonby (Auckland)

### Ingénieur réseau (secteur ferroviaire), Orange Business Services
#### Mars 2008 - Août 2009

* Conception, développement et intégration d'un contrôleur d'accès embarqué pour routeurs Mobile IP, projet Connexion TGV.
* Encadrement et formation de l'équipe de validation système

## Formations

* EFAB: Master 2 Stratégies économiques, numérique et données (SEND) (2021 - 2022)
* CNAM: Master 2 Statistique du risque pour la finance et l’assurance (2019 - cursus supprimé en 2021)
* CNAM: Master 1 Mathématiques appliquées et statistiques (2018 - 2021)
* CNAM: Licence générale mention Mathématiques Appliquées (2018 - 2019)
* Udacity: Machine Learning Engineer Nanodegree (Juillet 2018 - Octobre 2018)
* Ulm Universität, Allemagne: Communications Technology (Septembre 2006 - Avril 2007)
* ENSSAT, France: Diplôme d'ingénieur EII (2004 - 2007)
