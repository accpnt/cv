# Tanguy Mervin

Senior Data Scientist • +33 6 23 06 23 79 • Paris • [tmervin0011@icloud.com](mailto:tmervin0011@icloud.com) • [GitLab](https://gitlab.com/accpnt) • [Blog](https://accpnt.eu/)

## Profile

Accomplished and results-oriented Senior Data Scientist with proven expertise in developing, testing, and industrializing algorithms within the dynamic realm of data analytics. My objective is to leverage a unique blend of data science proficiency and extensive industry knowledge to enhance customer performance and experience in maintenance, operations, energy, and city flow domains. 

## Skills

* Technical: SQL, Splunk, Grafana, Kafka, Clickhouse SQL, Python, R, C and C++
* Functional: data analysis, visualization and modelling, applied mathematics and statistics, machine learning
* Miscellaneous: curiosity, problem-solving, critical thinking

## Experience

::: {.spread}
### QA Data Specialist - Kaiko
#### December 2023 - present
:::

* Provide quality assurance strategies for cryptocurrency data pipelines and validation of quantitative models.
* Implement time series-based implied volatility, indices, and analytics models testing pipelines.
* Technical Skills: Python, Clickhouse, SQL, Rust

::: {.spread}
### Data Expert - Freelance
#### November 2021 - December 2023
:::

* Data Expert consultant for Siemens Mobility (June 2022 - November 2023)
    * Conduct data analysis and engineering for NExTEO, ATS+, and PL14 projects.
    * Provide insights on railway signalization data through client-driven dashboards.
    * Technical Skills: Splunk, Grafana, Kafka, Clickhouse SQL, Python
* IoT architect for Beerlink (April 2022 - July 2022)
* Software architect for New Imaging Technologies (January 2022 - April 2022)

::: {.spread}
### Data Scientist - DeepLime
#### January 2020 - August 2021
:::

* Developed a spatial statistics Python library.
    * Conducted 3D variography, kriging estimation, uniform conditioning modeling.
    * Applied open pit calculus with graph theory and linear algebra.
    * Validated algorithms through unit tests.
* Technical Skills: Python, R, CI/CD

::: {.spread}
### Software engineer - Siemens Mobility
#### April 2018 - January 2020
:::

* Developed test simulators for public transportation in the Greater Paris area.
* Provided insights on simulation data for the NExTEO project.
* Technical Skills: C, C++, Python, Splunk, Wireshark

::: {.spread}
### Technical support engineer - Freelance
#### June 2017 - December 2017
:::

---

::: {.spread}
### Software Engineer - Elsys Design
#### August 2010 - February 2017
:::

---

::: {.spread}
### Working holiday in New Zealand
#### August 2009 - May 2010
:::

---

::: {.spread}
### Software Engineer - Orange Business Services
#### March 2008 - August 2009
:::

---

## Education

* EFAB: Master's degree in Economics and data analysis (SEND) (2021 - 2022)
* CNAM: Bachelor of Honor in Quantitative analysis and risk statistics for finance (2018 - 2021)
* CNAM: Bachelor Applied mathematics and statistics (2018 - 2019)
* Ulm Universität, Germany: Master 2 Communications Technology (2006 - 2007)
* ENSSAT, France: Engineering Master's degree (2004 - 2007)

## Projects

* [Time series smoothing library in Rust](https://gitlab.com/accpnt/smooth)
* Open core modules in VHDL [SHA-1](https://gitlab.com/accpnt/sha1crack) et [DMX](https://gitlab.com/accpnt/dmx)
* [Statistical analysis, models and reports for CNAM lectures](https://gitlab.com/accpnt/cnam)
