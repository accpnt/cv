::: {.center}
# Tanguy **Mervin**

[tmervin0012@icloud.com](mailto:tmervin0012@icloud.com) • +33623062379 • Projets personnels sur [GitLab](https://gitlab.com/accpnt) • Paris, France
:::

---

## IT Quant

Statisticien spécialisé en stratégies économiques et mathématiques appliquées avec une option en risque pour la finance. Quatre ans d'expérience dans la transformation de données brutes en informations stratégiques, comprenant une récente immersion dans la finance décentralisée. Compétent en gestion des risques financiers et en modélisation quantitative, avec une solide capacité à déployer des outils analytiques.

## Expérience

### Analyste de données Senior
#### SNCF Immobilier
##### Avril 2024 - présent

* Suivi et coordination des projets de développement data au sein du projet NOTITIA, avec un focus sur l'analyse de données financières et immobilières pour alimenter des tableaux de bord d'indicateurs 

### Analyste de données blockchain
#### Kaiko
##### Décembre 2023 - Avril 2024

* Implémentation de pipelines de test pour la validation des flux de données issus de différents exchanges crypto, du calcul de la volatilité implicite des options et des indices financiers (Python, Clickhouse, SQL, Rust)

### Analyste de données
#### Freelance pour Siemens Mobility
##### Novembre 2021 - Décembre 2023

* Spécialiste Data pour Siemens Mobility (juin 2022 - décembre 2023)
    * Analyse de données volumineuses pour l'identification des anomalies de fonctionnement et des axes d'amélioration de la circulation des trains CBTC du projet NExTEO/ATS+ (Splunk, Python)
    * Intégration d'un système de gestion de données massives (Apache Kafka, telegraf, Clickhouse SQL, Prometheus)
* Architecte logiciel pour New Imaging Technologies (janvier 2022 - avril 2022)

### Statisticien
#### Deeplime
##### Janvier 2020 - Août 2021

* Collecte, nettoyage et traitement de données d'exploitation minières pour alimenter des analyses statistiques approfondies.
* Modélisation et développement d'une librairie de calcul en statistiques spatiales pour l'estimation de minerais (Python, R)
* Compétences transférables: calcul d'indicateurs, techniques de modélisation statistique

### Ingénieur systèmes de test
#### Siemens Mobility
##### Avril 2018 - Janvier 2020

### Ingénieur de support technique
#### Freelance
##### Juin 2017 - Décembre 2017

### Ingénieur de développement
#### Elsys Design
##### Août 2010 - Février 2017

### Working holiday en Nouvelle-Zélande
##### Août 2009 - Mai 2010

### Ingénieur de développement
#### Orange Business Services
##### Mars 2008 - Août 2009

## Formations

* EFAB: Master 2 Stratégies économiques, numérique et données (SEND) (2021 - 2022)
* CNAM: Master 1 Mathématiques appliquées et statistiques du risque pour la finance (2018 - 2021)
* ENSSAT: Diplôme d'ingénieur en Electronique et Informatique Industrielle (2004 - 2007)

## Compétences 

* Langages de programmation: SQL, Python, R, C/C++, Rust
* Statistiques et analyse quantitative: statistique du risque (EVT, Value-at-Risk), modélisation de volatilité (GARCH), stress testing (Monte Carlo)
* Finance de marché: produits de taux (options et futures)
