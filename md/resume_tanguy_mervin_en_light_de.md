# Tanguy Mervin

[tmervin0010@icloud.com](mailto:tmervin0010@icloud.com) • +33623062379 • [GitLab](https://gitlab.com/accpnt) • Paris, France

## Data Engineer

Data enthusiast with a strong background in mathematics and programming. With four years of experience in data, I have developed a deep understanding of data modeling, visualization and engineering, as well as proficiency in tools such as SQL and Python. I am seeking opportunities to apply my skills and knowledge to help organizations navigate the exciting and rapidly-evolving world of data. Former software and backend engineer. 

## Experience

::: {.spread}
### Data Analyst / Engineer
#### November 2021 - Current
:::
* Data Analyst / Engineer consultant for Siemens Mobility (june 2022 - current)
    * Data analysis and engineering for NExTEO, ATS+ and PL14 projects
    * Functional skills: providing insights on railway signalization data, through client-driven dashboards
    * Technical skills: Splunk, Grafana, Apache Kafka, Clickhouse SQL, Python, Linux
* IoT architect for Beerlink (april 2022 - july 2022)
* Software architect for New Imaging Technologies (january 2022 - april 2022)

::: {.spread}
### Data Engineer - Deeplime
#### January 2020 - August 2021
:::

* Analysis and extraction of mining datasets (Python)
* Developpment of a spatial statistics Python library, with algorithm validation through automated unit tests (Python, R, CI/CD)
* Training materials for spatial statistics computation (Python, Jupyter Notebook)

::: {.spread}
### Test systems engineer - Siemens Mobility
#### April 2018 - January 2020
:::

* Test simulators development for the Greater Paris area public railways (C/C++)
* Consultant for SNCF and RATP on the NExTEO project
* Data mining and analysis for simulation logs monitoring (Splunk)

::: {.spread}
### Technical support engineer - Freelance
#### June 2017 - December 2017
:::

---

::: {.spread}
### Software Engineer - Elsys Design
#### August 2010 - February 2017
:::

---

::: {.spread}
### Working holiday in New Zealand
#### August 2009 - May 2010
:::

---

::: {.spread}
### Software Engineer - Orange Business Services
#### March 2008 - August 2009
:::

---

## Education

* EFAB: Master 2 Economics and data analysis (SEND) (2021 - 2022)
* CNAM: Master 2 Risk statistics for finance and business (2019 - 2020)
* CNAM: Master 1 Applied mathematics and statistics (2018 - 2021)
* CNAM: Bachelor Applied mathematics and statistics (2018 - 2019)
* Ulm Universität, Germany: Master 2 Communications Technology (2006 - 2007)
* ENSSAT, France: Engineering Master's degree (2004 - 2007)

## Skills

* Technical: SQL, Splunk, Grafana, Apache Kafka, Clickhouse SQL, Python, R, Linux
* Functional: data analysis and engineering, backend development, visualization and modelling, applied mathematics and statistics
* Miscellaneous: curiosity, problem-solving, critical thinking

## Personal projects

* [Time series smoothing library](https://gitlab.com/accpnt/smooth) in Rust
* Design of [SHA-1](https://gitlab.com/accpnt/sha1crack) and [DMX](https://gitlab.com/accpnt/dmx) cores in VHDL
