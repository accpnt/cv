::: {.center}
# Tanguy **Mervin**

[tmervin0012@icloud.com](mailto:tmervin0012@icloud.com) • 0623062379 • Projets personnels sur [GitLab](https://gitlab.com/accpnt) • Paris, France
:::

---

## Spécialiste Data senior

Cinq ans d'expérience dans la data pour la transformation de données brutes en information stratégique. Compétences avancées en traitement et visualisation de données, modélisation statistique et intégration d'infrastructure pour la gestion de données massives. Expert dans l'utilisation d'outils tels que SQL et Python pour la résolution de problèmes complexes. 

## Expérience

### Spécialiste Data Senior
#### SNCF Immobilier
##### Avril 2024 - présent

* Suivi de projet et support aux équipes de développement data du projet NOTITIA, hub de données hébergé sur AWS
* Ingestion de données immobilières pour alimenter des tableaux de bord d'indicateurs financiers
* Architecture et développement d'un outil Python et SQL dédié au calcul du décret tertiaire sur les données de consommation du parc immobilier du parc SNCF
* **Stack: Databricks, AWS, Power BI**

### Data Analyst Blockchain
#### Kaiko
##### Décembre 2023 - Avril 2024

* Implémentation de pipelines de test pour la validation des flux de données et du calcul de la volatilité implicite et des indices financiers issus des différentes plateformes de trading crypto
* **Stack: Python, Clickhouse, SQL, Rust, Docker**

### Spécialiste Data
#### Freelance pour Siemens Mobility
##### Novembre 2021 - Décembre 2023

* Spécialiste Data pour Siemens Mobility (juin 2022 - décembre 2023)
    * Analyse de données volumineuses pour l'identification des anomalies de fonctionnement et des axes d'amélioration de la circulation des trains CBTC pour le projet NExTEO/ATS+
    * Création de tableaux de bord pour la visualisation de la demande future en exploitation et maintenance des produits Trainguard CBTC du projet. Remontée des anomalies de fonctionnement sous forme d'alertes intéractives
    * Développement de scripts d'agrégation de données pour mettre en évidence les KPI clés 
    * Collaboration étroite avec les équipes produit et système pour l'identification des défauts de fonctionnement et l'ajustement des stratégies de maintenance ferroviaire
    * Déploiement du produit ADONEM sur site à St Ouen et Pantin, support aux équipes MES et SIG Atos/SNCF
    * Intégration d'un système de stockage de données massives interne à Siemens
* Architecte IoT pour Beerlink (avril 2022 - juillet 2022)
* Architecte logiciel pour New Imaging Technologies (janvier 2022 - avril 2022)
* **Stack: Splunk, Python, Grafana, Apache Kafka, telegraf, Clickhouse SQL, Prometheus, Docker**

### Data Scientist
#### Deeplime
##### Janvier 2020 - Août 2021

* Collecte, nettoyage et traitement de données minières à partir de diverses sources (propriétaires et Open Data) pour alimenter des analyses statistiques approfondies d'exploitation minière.
* Modélisation et développement d'une librairie de calcul en statistiques spatiales pour l'estimation des minerais (variographie, krigeage, conditionnement uniforme)
* Développement d'algorithmes de calcul du pourtour optimal pour l'optimisation de fosse (théorie des graphes)
* Test unitaires pour l'intégration et la validation d'algorithmes de calcul géostatistiques
* Préparation de supports à la formation en statistiques spatiales
* **Stack: Python, R, React, Microsoft Azure**

### Ingénieur systèmes de test
#### Siemens Mobility
##### Avril 2018 - Janvier 2020

* Développement réseau sur les simulateurs de test SIMEV2 des métros automatisés Trainguard CBTC (projet NExTEO)
    * Simulation des circuits de voie, balises et signaux de signalisation
* Portage BSP de la carte A15 vers A21 (QorIQ) de chez MEN Mikroelectronik pour la simulation de l'odométrie, les calculs Digisafe et la communication CIP sur baies de test
* Intégration, support technique et suivi de chantier avec les partenaires SNCF et RATP pour le projet NExTEO
* Analyse de données et traitement de logs de simulation ferroviaire
* **Stack: C, C++, Python, Lua, Wireshark, Splunk, vxWorks RTOS**

### Ingénieur de support technique
#### Freelance
##### Juin 2017 - Décembre 2017

* Support système, réseau et base de données pour une création d’entreprise 
* **Stack: Python, Linux, PostgreSQL**

### Ingénieur de développement
#### Elsys Design
##### Août 2010 - Février 2017

* Conception, développement et intégration des logiciels embarqués et FPGA de systèmes modulaires dédiés au caméras thermiques de Sagem Argenteuil. Développement sur cibles MSP430, NIOS II, STM32, OMAP3530 et en VHDL sur FPGA Altera.
* Intégration réseau sur routeurs pour Sagemcom
* Développement d’un testbench, bootloader et interface Telnet pour un système électronique et logiciel temps-réel de ﬁltrage d’eau pour Millipore
* **Stack: C, C++, Shell, FreeRTOS, Linux, Python**

### Working holiday en Nouvelle-Zélande
##### Août 2009 - Mai 2010

### Ingénieur réseau
#### Orange Business Services
##### Mars 2008 - Août 2009

* Développement, conception, intégration et validation d'un contrôleur d'accès embarqué pour routeurs Mobile IP à haute capacité de handover (WiFi, satellite, 3G) dans le cadre du projet Connexion TGV pour la SNCF
* Intégration et administration système sur la plateforme de maintenance TGV Est
* Formation de l’équipe de test et de maintenance pour le déploiement du système sur site. 
* Déploiement effectif du produit sur le site SNCF de l’Ourcq à Paris pour les 50 trains du TGV Est en octobre 2009
* **Stack: C, C++, OpenSSL, système et noyau Linux, TCP/IP**

## Formations

* EFAB: Master 2 Stratégies économiques, numérique et données (SEND) (2021 - 2022)
* CNAM: Master 1 Mathématiques appliquées et statistiques (2018 - 2021)
* CNAM: Licence générale mention Mathématiques Appliquées (2018 - 2019)
* Ulm Universität: Master 2 Communications Technology (Septembre 2006 - Avril 2007)
* ENSSAT: Diplôme d'ingénieur en Electronique et Informatique Industrielle (2004 - 2007)

## Compétences 

* Langages: SQL, Python, R, C/C++, Rust
* Stockage des données: PostgreSQL, Clickhouse, SQL, Splunk, Apache Kafka
* Outils: Grafana, Tableau, Confluent telegraf, Prometheus, Git, Wireshark
* Statistiques: modélisation géostatistique, tests hypothétiques, régression
* Manipulation de données: nettoyage, transformation, agrégation, ingestion
* Fonctionnel: analyse de données approfondie, communication efficace des résultats, résolution de problèmes complexes
