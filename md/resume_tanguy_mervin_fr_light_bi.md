# Tanguy Mervin

[tmervin0010@icloud.com](mailto:tmervin0010@icloud.com) • 0623062379 • [GitLab](https://gitlab.com/accpnt) • [Blog](https://accpnt.eu/) • Paris

## Business data analyst

Quatre ans d'expérience dans l'analyse de données et la génération d'insights exploitables pour aider les entreprises à prendre des décisions éclairées. Compétences avancées en manipulation de données, modélisation statistique et visualisation. Expert dans l'utilisation d'outils tels que SQL et Python pour la résolution de problèmes complexes et la transformation de données brutes en informations stratégiques. 

## Expérience

::: {.spread}
### Analyste de données freelance - Siemens Mobility
#### Novembre 2021 - Présent
:::
* Data Analyst pour Siemens Mobility (juin 2022 - présent)
    * Analyse de données volumineuses pour identifier les anomalies de fonctionnement et les axes d'amélioration de la circulation des trains CBTC pour le projet NExTEO/ATS+
    * Création de tableaux de bord interactifs pour visualiser la demande future en exploitation et maintenance des produits Trainguard CBTC (Splunk, Grafana)
    * Conception et développement de scripts d'agrégation de données pour mettre en évidence les KPI clés (Python)
    * Collaboration étroite avec les équipes produit et système pour identifier les défauts de fonctionnement et ajuster les stratégies de maintenance ferroviaire en conséquence
* Architecte IoT pour Beerlink (avril 2022 - juillet 2022)
* Architecte logiciel pour New Imaging Technologies (janvier 2022 - avril 2022)

::: {.spread}
### Analyste de données / statisticien - Deeplime
#### Janvier 2020 - Août 2021
:::

* Collecte, nettoyage et traitement de données minières à partir de diverses sources (propriétaires et Open Data) pour alimenter des analyses statistiques approfondies.
* Modélisation pour le développement d'une librairie de calcul en statistiques spatiales pour l'estimation des ressources minières
* Validation des algorithmes et modèles statistiques par tests unitaires
* Préparation de supports à la formation en modélisation statistique

::: {.spread}
### Ingénieur systèmes de test - Siemens Mobility
#### Avril 2018 - Janvier 2020
:::

* Développement réseau sur les simulateurs de test des métros automatisés CBTC (projet NExTEO)
* Data mining, analyse de données et traitement de logs de simulation ferroviaire (Splunk, Python)
* Reporting et gestion de projet client pour la SNCF

::: {.spread}
### Ingénieur de support technique - Freelance
#### Juin 2017 - Décembre 2017
:::

---

::: {.spread}
### Ingénieur de développement - Elsys Design
#### Août 2010 - Février 2017
:::

---

::: {.spread}
### Working holiday en Nouvelle-Zélande
#### Août 2009 - Mai 2010
:::

---

::: {.spread}
### Ingénieur de développement - Orange Business Services
#### Mars 2008 - Août 2009
:::

---

## Formations

* EFAB: Master 2 Stratégies économiques, numérique et données (SEND) (2021 - 2022)
* CNAM: Master 2 Statistique du risque pour la finance et l’assurance (2019 - cursus supprimé en 2021)
* CNAM: Master 1 Mathématiques appliquées et statistiques (2018 - 2021)
* CNAM: Licence générale mention Mathématiques Appliquées (2018 - 2019)
* Ulm Universität: Master 2 Communications Technology (Septembre 2006 - Avril 2007)
* ENSSAT: Diplôme d'ingénieur en Electronique et Informatique Industrielle (2004 - 2007)

## Compétences techniques et fonctionnelles

* Langages : SQL, Python, R
* Outils d'Analyse : Splunk, Grafana, Tableau
* Statistiques : modélisation, tests hypothétiques, régression
* Manipulation de données : nettoyage, transformation, agrégation
* Stratégies économiques et business: analyse sectorielle, design économique, économie numérique
* Fonctionnel: analyse de données approfondie, communication efficace des résultats, résolution de problèmes complexes

## Projets personnels

* Création d'une [librairie de lissage de séries temporelles financières](https://gitlab.com/accpnt/smooth) en Rust
