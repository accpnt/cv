::: {.center}
# Tanguy **Mervin**

[tmervin0012@icloud.com](mailto:tmervin0012@icloud.com) • +33623062379 • [GitLab](https://gitlab.com/accpnt) • Paris, France
:::

---

## IT Quant

Skilled in statistical analysis, data modeling, and financial engineering, I bring a strong technical foundation in Python, SQL, and C++ to support research, modeling, and optimization in quantitative finance. With experience spanning risk modeling, data pipeline optimization, and advanced algorithm development, I am dedicated to delivering innovative solutions that enhance trading strategies and data-driven decision-making. Passionate about leveraging quantitative expertise to tackle complex financial problems in dynamic environments.

## Experience

### Senior Data Scientist
#### SNCF Immobilier
##### April 2024 - current

* Manage and coordinate data development initiatives focusing on financial and real estate analytics
* Develop and maintain indicator dashboards to support data-driven decision-making.

### Blockchain data analyst
#### Kaiko
##### December 2023 - April 2024

* Designed and implemented QA strategies for cryptocurrency data pipelines.
* Validated quantitative models, including implied volatility indices and market indices.
* Technical skills: Python, Clickhouse, SQL, Rust

### Data Analyst
#### Freelance for Siemens Mobility
##### November 2021 - December 2023

* Data Consultant (June 2022 – November 2023):
    * Delivered insights on railway signaling data using customized dashboards.
    * Developed data analysis tools for NExTEO, ATS+, and PL14 projects.
    * Technical tools: Splunk, Grafana, Kafka, Clickhouse SQL, Python
* IoT Architect for Beerlink (April 2022 – July 2022): developed IoT architecture and solutions.
* Software Architect for New Imaging Technologies (January 2022 – April 2022): designed and implemented software solutions.

### Data Scientist
#### DeepLime
##### January 2020 - August 2021

* Developed a spatial statistics Python library for geostatistical analysis.
    * Modeled 3D variograms, kriging, and uniform conditioning for resource estimation.
    * Applied graph theory and linear algebra to optimize open-pit designs.
    * Validated algorithms through unit testing and CI/CD pipelines.
* Technical skills: Python, R, CI/CD

### Software engineer
#### Siemens Mobility
##### April 2018 - January 2020

* Functional: develop test simulators for the Greater Paris area public railways, provide insights on simulation data for the NExTEO project
* Technical skills: C, C++, Python, Splunk, Wireshark 

### Software engineer
#### Freelance
##### June 2017 - December 2017

### Software engineer
#### Elsys Design
##### August 2010 - February 2017

### Working holiday in New Zealand
##### August 2009 - May 2010

### Software engineer
#### Orange Business Services
##### March 2008 - August 2009

## Education

* EFAB: Master's degree in Economics and data analysis (SEND) (2021 - 2022)
* CNAM: Bachelor of Honor in Quantitative analysis and risk statistics for finance (2018 - 2021)
* CNAM: Bachelor Applied mathematics and statistics (2018 - 2019)
* Ulm Universität, Germany: Master 2 Communications Technology (2006 - 2007)
* ENSSAT, France: Engineering Master's degree (2004 - 2007)

## Skills 

* Programming languages: Python, SQL, R, C/C++, Rust
* Statistical analysis: EVT, Value-at-Risk (VaR), GARCH modeling, stress testing (Monte Carlo simulations)
* Financial engineering: derivatives pricing (options, futures), volatility modeling, portfolio risk metrics
* Tools & technologies: Splunk, Grafana, Clickhouse, Kafka, CI/CD pipelines
* Domain expertise: cryptocurrency markets, spatial statistics
