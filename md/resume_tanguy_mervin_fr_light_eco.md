::: {.center}
# Tanguy **Mervin**

[tmervin0011@icloud.com](mailto:tmervin0011@icloud.com) • 0623062379 • Projets personnels sur [GitLab](https://gitlab.com/accpnt) • Paris, France
:::

---

## Consultant en stratégies économiques

Quatre ans d'expérience en analyse de données pour la transformation de données brutes en information stratégique. Après dix ans d'expérience comme consultant, je souhaite aujourd'hui mettre à profit mes compétences en stratégies économiques au service des clients. 

## Expérience

### Analyste de données
#### Kaiko
##### Décembre 2023 - Présent

* Validation des flux de données financières pour alimenter des modèles quantitatifs (Python, Clickhouse, SQL, Rust)

### Consultant en analyse de données
#### Freelance pour Siemens Mobility
##### Novembre 2021 - Décembre 2023

* Spécialiste Data pour Siemens Mobility (juin 2022 - décembre 2023)
    * Analyse de données volumineuses pour identifier les anomalies de fonctionnement et les axes d'amélioration de la circulation des trains CBTC pour le projet NExTEO/ATS+ (Splunk, Python)
    * Collaboration étroite avec les équipes produit et système pour l'identification des défauts de fonctionnement et l'ajustement des stratégies de maintenance ferroviaire
    * Intégration d'un système de gestion de données massives (Apache Kafka, telegraf, Clickhouse SQL, Prometheus)
* Architecte IoT pour Beerlink (avril 2022 - juillet 2022)
* Architecte logiciel pour New Imaging Technologies (janvier 2022 - avril 2022)

### Spécialiste data
#### Deeplime
##### Janvier 2020 - Août 2021

* Collecte, nettoyage et traitement de données minières à partir de diverses sources (propriétaires et Open Data) pour alimenter des analyses statistiques approfondies d'exploitation minière.
* Modélisation et développement d'une librairie de calcul en statistiques spatiales pour l'estimation des minerais (Python, R)

### Ingénieur systèmes de test
#### Siemens Mobility
##### Avril 2018 - Janvier 2020

* Développement sur les simulateurs de test des métros automatisés CBTC (projet NExTEO)
* Analyse de données et traitement de logs de simulation ferroviaire (Splunk, Python)

### Ingénieur de support technique
#### Freelance
##### Juin 2017 - Décembre 2017

### Consultant en développement
#### Elsys Design
##### Août 2010 - Février 2017

### Working holiday en Nouvelle-Zélande
##### Août 2009 - Mai 2010

### Consultant en développement
#### Orange Business Services
##### Mars 2008 - Août 2009

## Formations

* EFAB: Master 2 Stratégies économiques, numérique et données (SEND) (2021 - 2022)
* CNAM: Master 1 Mathématiques appliquées et statistiques (2018 - 2021)
* CNAM: Licence générale mention Mathématiques Appliquées (2018 - 2019)
* Ulm Universität: Master 2 Communications Technology (Septembre 2006 - Avril 2007)
* ENSSAT: Diplôme d'ingénieur en Electronique et Informatique Industrielle (2004 - 2007)

## Compétences 

* Fonctionnel: reporting client, conseil en entreprise, stratégies économiques et sociales, analyse de données financières et industrielles
* Statistiques: modélisation, tests hypothétiques, régression
