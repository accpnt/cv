# Tanguy Mervin

Statisticien • +33 (0)6 23 06 23 79 • [tmervin0008@icloud.com](mailto:tmervin0008@icloud.com) • [GitLab](https://gitlab.com/accpnt) • Paris

## Compétences

* Analyse de données: interfaçage API, web scraping, aggrégation et reporting, SQL, Tableau
* Data science : traitement de données (R, Python), data visualisation (plotly, matplotlib)
* Electronique: FPGA (Altera), ModelSim, instrumentation
* Informatique: Python, R, C/C++, Rust
* Mathématiques appliquées: géostatistiques, théorie des graphes, modélisation et calcul matriciel
* Réseau et bases de données : Linux, Git, iptables, Wireshark, PostgreSQL

## Formations

* EFAB: Master 2 Stratégies économiques, numérique et données (SEND) (2021 - 2022)
* CNAM: Master Statistique du risque pour la finance et l’assurance (2018 - présent)
* CNAM: Licence générale mention Mathématiques Appliquées (2018 - 2019)
* Udacity: Machine Learning Engineer Nanodegree (Juillet 2018 - Octobre 2018)
* CNAM: Biostatistiques (Février 2017 - Juin 2017)
* CNAM Entreprise: Design Thinking (Novembre 2016)
* Ulm Universität, Allemagne: Master 2 Communications Technology (Septembre 2006 - Avril 2007)
* ENSSAT Lannion, France: Ingénieur Electronique et Informatique Industrielle (2004 - 2007)

## Expériences

### Consultant
#### Freelance
##### Novembre 2021 - présent

* Architecture logicielle pour New Imaging Technologies
* Architecture IoT pour Beerlink

### Géostatisticien / Data Scientist
#### DeepLime
##### Janvier 2020 - Août 2021

* Optimisation de fosse: développement d'algorithmes de calcul du pourtour optimal, théorie des graphes (Python)
* Développement d'une librairie de géostatistiques: variographie 3D, estimation par krigeage (Python: pandas, numpy)
* Développement frontend (React.js) sur un portail de calcul numérique et de visualisation 3D d'objets miniers (GIS)
* Test unitaires pour l'intégration et la validation d'algorithmes de géostatistiques (R, Python)
* Réécriture d'une procédure d'interpolation spatiale Isatis (Python)
* Préparation de supports à la formation en géostatistiques

### Ingénieur systèmes de test
#### Siemens
##### Avril 2018 - Janvier 2020

* Portage de BSP de la carte A15 vers A21 (QorIQ) de chez MEN Mikroelectronik
* Développement réseau sur les simulateurs de test des métros automatisés CBTC
* Data mining sur données férroviaires à l'aide de l'outil splunk
* Intégration de node.js en tant que librairie dans un simulateur de trafic CBTC en C++

### Data scientist
#### Freelance
##### Juin 2017 - Décembre 2017

* Conception et implémentation d’un module Python pour l’automatisation du modèle ARIMA mono variable
* Intégration de pyramid-arima pour l’automatisation des modélisations ARIMA
* Support système Linux, réseau et PostgreSQL pour une création d’entreprise

### Ingénieur systèmes embarqués
#### Elsys Design
##### Août 2010 - Février 2017

* Conception, développement et intégration des logiciels embarqués et FPGA de systèmes modulaires dédiés au caméras thermiques de Sagem Argenteuil. Développement en C sur cibles MSP430, NIOS II, STM32, OMAP3530 et en VHDL sur FPGA Altera.
    * En tant qu'ingénieur logiciel embarqué :
        * Architecture et développement du framework de driver pour systèmes NIOS embarqués
        * Architecture, conception et développement d’outils de gestion des modes vidéo (Python)
        * Développement et intégration sur le logiciel de boot de secours, U-Boot and les drivers Linux d’une carte intégrant un OMAP3530. Particulièrement sur le développement d’un driver RS-232 propriétaire et d’un gadget USB composite permettant l’échange de données et le streaming UVC
        * Développement et maintenance d’un logiciel pour Altera NIOS II et de drivers pour la gestion du PAL/OLED, ainsi que pour l’afﬁchage zoomé, picture-in-picture et split-screen des ﬂux IR et IL
        * Développement et intégration d’un logiciel NIOS II pour la gestion des cartes de proximité IR
        * Développement d’un logiciel basé sur FreeRTOS pour la gestion des cartes MSP430 en charge de la restitution OLED du cœur vidéo
    * En tant qu'ingénieur FPGA :
        * Intégration et validation du FPGA de test pour mainboard K3 (Transceiver Toolkit, Quartus). Intégration d’un module HD-SDI module utilisant les IPs transceiver Altera
        * Développement et integration FPGA sur un SoC de test pour la carte mère de gestions des boules gyro-stabilisées
        * Debug du module VHDL PAL BT656 à l’aide de SignalTap
* Intégration réseau sur routeurs pour Sagemcom
    * Environnement technique : Linux, C, shell scripting
* Développement d’un testbench, bootloader et interface Telnet pour un système électronique et logiciel temps-réel de ﬁltrage d’eau pour Millipore (C, Coldﬁre)

### Working holiday en Nouvelle-Zélande
##### Août 2009 - Mai 2010

* Serveur au Café 121 à Ponsonby, Auckland
* Consolidation de mon niveau d'anglais

### Ingénieur réseau
#### Orange Business Services
##### Mars 2008 - Août 2009

* En tant que membre du projet Connexion TGV (Internet haut-débit sur TGV), j’ai développé, conçu et intégré en tant qu’ingénieur télécommunications un contrôleur d'accès embarqué pour routeurs Mobile IP à haute capacité de handover (WiFi, satellite, 3G)
    * Cette application client/serveur légère et multi-threadée permet les fonctionnalités suivantes :
        * Pare-feu et contrôle d'accès pour permettre aux usagers connectés au train d'accéder à différents types de services en ligne (C, Linux, netﬁlter, libiptc)
        * Portail captif pour la redirection des usagers connectés au WLAN
        * Redirections des protocoles DNS, HTTP, HTTPS et proxy. (OpenSSL)
        * Contrat d'interface UDP pour le contrôle d'applications embarquées
        * Sonde fournissant des statistiques sur l'usage en bande passante par utilisateur et par service (Internet, VoD).
    * Au cours du développement de cette application, mes responsabilités incluaient :
        * Rédaction des spéciﬁcations techniques et fonctionnelles de l'application embarquée, en suivant le style FT R&D.
        * Maintenance et packaging du proxy et des modules noyau Linux sur système Debian (TPROXY, Squid)
        * Intégration et administration du système sur la plateforme de maintenance TGV Est. Pendant cette période, j’ai corrigé ou reporté des erreurs de conﬁguration automatique ou de dépendance
        * Formation de l’équipe de test et de maintenance sur le ﬁltrage de paquet et administration système sur plateforme UNIX aﬁn de préparer le déploiement du système sur site. Suite à ma participation à l’intégration globale du système sur le site SNCF de l’Ourcq à Paris, le déploiement a été effectué sur les lignes du TGV Est (50 trains) en octobre 2009
* Maintenance et développement d’extensions Wireshark pour la visualisation de données géographiques militaires sensibles sur les réseaux internes et externes des frégates FREMM pour Thalès. Mes responsabilités incluaient :
    * idl2wrs, un générateur de code permettant la création automatique de dissecteurs Wireshark en C a partir de sources CORBA (IDL). J'ai conçu un module Python permettant la génération de fonctionnalités de dissections supplémentaires.
    * Dissecteur FREMM existant adaptés au support de TCP et supportant l'extraction et la visualisation en temps-réel d'attributs spéciﬁques déclenchant des évènements asynchrones
    * Outils de test client/serveur pour la génération de paquets TCP à partir de paquets FREMM UDP
    * Environnement technique : C, Python, C++, Linux (virtualisé, RedHat)

## Stages

### Ingénieur en traitement d’image
#### Hôpital Tenon - APHP
##### Mars 2007 - Août 2007

Stage de fin d’études sur la visualisation 3D de tumeurs prostatiques sous Matlab. Responsable de la conception et du développement d'outils de visualisation pour une unité de recherche médicale spécialisée dans le cancer de la prostate. En tant qu'ingénieur de support, mes responsabilités incluaient :

* Déterminer un nouveau modèle de visualisation 3D de la tumeur prostatique à partir de résultats anatomopathologiques de biopsies prostatiques. Une interface preuve de concept a été conçue pour les praticiens, permettant la visualisation interactive du modèle sous Matlab
* Prototypage d'un modèle physique de déformation 3D émulant une évolution possible de la surface tumorale au cours du temps (OpenGL, C++)
* Implémentation   d'un   filtre   pour   la   réduction   du   speckle   noise   d'images échographiques (TRUS) de la prostate (sources Matlab du filtre disponibles sur Mathworks)

### Web Designer
#### Washington Navy Museum
##### Eté 2002

* Refonte du site web du musée : re-conceptualiser la présentation des expositions sur le site web du musée et proposer un design en accord avec la charte graphique du département (HTML, CSS)
* Ajout des nouvelles expositions : photographie et scan des artefacts pertinents, rédaction des résumés d'exposition (Photoshop)

## Référents

Disponibles sur demande.
