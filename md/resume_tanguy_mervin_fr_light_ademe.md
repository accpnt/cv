::: {.center}
# Tanguy **Mervin**

[tmervin0012@icloud.com](mailto:tmervin0012@icloud.com) • 0623062379 • Projets personnels sur [GitLab](https://gitlab.com/accpnt) • Paris, France
:::

---

## Chef de projet data

Ingénieur puis statisticien suite à une mobilité fonctionnelle en stratégies économiques et mathématiques appliquées. Quatre ans d'expérience dédiée à la transformation de données brutes en information stratégique, avec une expérience récente en pilotage de projet data. Passionné par l'univers des low-tech. 

## Expérience

### Chef de projet data
#### SNCF
##### Avril 2024 - présent

* Suivi, pilotage de projet et support technique aux équipes de développement data du projet NOTITIA (Databricks)

### Analyste de données
#### Kaiko
##### Décembre 2023 - Avril 2024

* Implémentation de pipelines de test pour la validation des flux de données et du calcul de la volatilité implicite et des indices financiers (Python, Clickhouse, SQL, Rust)

### Spécialiste Data
#### Freelance pour Siemens Mobility
##### Novembre 2021 - Décembre 2023

* Spécialiste Data pour Siemens Mobility (juin 2022 - décembre 2023)
    * Analyse de données volumineuses pour l'identification des anomalies de fonctionnement et des axes d'amélioration de la circulation des trains CBTC du projet NExTEO/ATS+ (Splunk, Python)
    * Intégration d'un système de gestion de données massives (Apache Kafka, telegraf, Clickhouse SQL, Prometheus)
* Architecte IoT pour Beerlink (avril 2022 - juillet 2022)
* Architecte logiciel pour New Imaging Technologies (janvier 2022 - avril 2022)

### Statisticien
#### Deeplime
##### Janvier 2020 - Août 2021

* Collecte, nettoyage et traitement de données d'exploitation minières pour alimenter des analyses statistiques approfondies.
* Modélisation et développement d'une librairie de calcul en statistiques spatiales pour l'estimation de minerais (Python, R)

### Ingénieur systèmes de test
#### Siemens Mobility
##### Avril 2018 - Janvier 2020

### Ingénieur de support technique
#### Freelance
##### Juin 2017 - Décembre 2017

### Ingénieur de développement
#### Elsys Design
##### Août 2010 - Février 2017

### Working holiday en Nouvelle-Zélande
##### Août 2009 - Mai 2010

### Ingénieur de développement
#### Orange Business Services
##### Mars 2008 - Août 2009

## Formations

* EFAB: Master 2 Stratégies économiques, numérique et données (SEND) (2021 - 2022)
* CNAM: Master 1 Mathématiques appliquées et statistiques (2018 - 2021)
* CNAM: Licence générale mention Mathématiques Appliquées (2018 - 2019)
* Ulm Universität: Master 2 Communications Technology (Septembre 2006 - Avril 2007)
* ENSSAT: Diplôme d'ingénieur en Electronique et Informatique Industrielle (2004 - 2007)

## Compétences 

* Statistiques: modélisation, tests hypothétiques, régression, modélisation de volatilité sur séries temporelles
* Economie: analyse sectorielle, design économique, économie numérique
