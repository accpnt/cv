# Tanguy Mervin

Statistician • +33 (0)6 23 06 23 79 • [tmervin0009@icloud.com](mailto:tmervin0009@icloud.com) • [GitLab](https://gitlab.com/accpnt) • Paris

## Summary

> Holds a Master Degree in Electronics and Embedded Systems from ENSSAT Lannion (France). Nine years of experience in embedded systems, mostly software but also FPGA. [Open source developer](https://gitlab.orgcom/accpnt/). Fluent English (Cambridge Proficiency in English). Currently switching to statistics and data science.

## Education

* Udacity: Machine Learning Engineer Nanodegree, July 2018 - December 2018
* CNAM Paris, France : Master's degree in statistics and data science (2017 - ongoing)
* ENSSAT Lannion, France : Master's degree in electronics and embedded systems (2004 - 2007)
* Ulm Universität, Germany : Communications Technology (September 2006 - April 2007)

## Skills

* Languages : Python, R, SAS, C/C++, VHDL, Bash, Qt
* Data science : pandas, statsmodels, scikit-learn, pyramid-arima, Bokeh
* Networking and databases : Linux, Git, iptables, Wireshark, PostgreSQL

## Experience

### Data Analyst
#### Freelance
##### November 2021 - current

* Data Analyst consultant for Siemens Mobility (june 2022 - current)
    * Data analysis for NExTEO, ATS+ and PL14 projects
    * Functional skills: providing insights on railway signalization data, through client-driven dashboards
    * Technical skills: Splunk, Grafana, Kafka, Clickhouse SQL, Python
* IoT architect for Beerlink (april 2022 - july 2022)
* Software architect for New Imaging Technologies (january 2022 - april 2022)

### Data Scientist
#### DeepLime
##### January 2020 - August 2021

* Revamp of backed architecture (Python based) for an interpolation procedure for geological data.
* Opensource contributions on geostatsmodels (autocorrelation metrics)
* Kriging algorithm adaptation using Gibbs sampling
* Microsoft Azure cloud micro-functions for data manipulation

### Embedded systems engineer
#### Siemens
##### April 2018 - January 2020

* BSP porting of MEN Mikroelectronik A15 boards to A21 QorIQ boards. VxWorks upgrade on A21 embedded software.
* Networking development on test supply software for CBTC trains.
* Log analysis and scraping using Python and pandas
* Simulation model development for NExTEO project
* Application-specific Lua dissectors for Wireshark

### Data scientist
#### Freelance
##### June 2017 - December 2017

* Design and development of a Python module for automated processing of mono variable ARIMA model, linear regression (OLS) and Monte Carlo (statsmodels, pandas).
* Integration of pyramid-arima for automated ARIMA models.
* Polynomial regression via scikit-learn.
* Plot rendering using Bokeh.
* Statistics, networking, Linux (Debian) and PostgreSQL support for an emerging company.

### Embedded systems engineer
#### Elsys Design
##### August 2010 - February 2017

* Design, development and integration on FPGAs and embedded software for modular systems dedicated to Sagem’s thermic cameras.
    * As and embedded software engineer :
        * Architecture and development of a driver framework for NIOS II embedded systems.
        * Architecture, design and development of video mode management tools (Python)
        * Development and integration on a fallback boot software (U-Boot) and Linux drivers of an OMAP3530 board. Particularly on a RS-232 driver and an USB composite gadget allowing data exchange and UVC streaming.
        * Development and maintenance of NIOS II software and drivers for PAL/OLED modes, as well as zoomed, picture-in-picture and split-screen of IR et IL video modes.
        * Development and integration of a NIOS II software for IR proximity boards.
        * FreeRTOS development on a MSP430 board in charge of OLED display.
    * As a FPGA engineer :
        * Integration and validation of a FPGA testbench for the K3 mainboard (Transceiver Toolkit, Quartus). HD-SDI module integration using Altera transceiver IPs.
        * FPGA development and integration of a SoC testbench gyro-stabilized mainboard management.
        * Debug of a PAL BT656 VHDL moduel using SignalTap
* Networking integration on Sagemcom's routers
    * Technical environment : Linux, C, shell scripting.
* Testbench, bootloader and Telnet interface development for an electronic and real-time water filtering system for Millipore (C, Coldﬁre)

### Working holiday in New Zealand
##### August 2009 - May 2010

Waiter at Café 121 in Ponsonby, Auckland.

### Network engineer
#### Orange Business Services
##### March 2008 - August 2009

* As a member of the Connexion TGV project (high speed Internet on TGvs), I developed, designed and integrated an embedded access controller software for Mobile IP routers with high handover capabilities (WiFi, satellite, 3G).
    * This lightweight client/server, multi-threaded software included :
        * Firewalling and access control allowing the train's wifi users to access several online services (C, Linux, netﬁlter, libiptc).
        * Captive portal for WLAN users redirection.
        * DNS, HTTP, HTTPS and proxy redirections (OpenSSL).
        * UDP messaging with the embedded portal.
        * Probe supplying statistics on bandwidth usage per user and per service (Internet, VoD).
    * During this software development, my responsibilites included :
        * Technical and functional specifications, following FT R&D guidelines.
        * Maintenance and packaging of the proxy Linux kernel modules on an embedded Debian system (TPROXY, Squid).
        * Integration and management in the SNCF maintenance center in Ourcq, Paris. Bug report and correction.
        * Training of the testing team on UNIX system and software validation.
	* The entire system has been successfully deployed on 50 trains of the East TGV lines.
* Maintenance and development of Wireshark extensions for geographical data visualization on internal and external FREMM networks for Thalès. My responsibilities included :
    * idl2wrs, a Wireshark dissector C code generator written in Python form CORBA (IDL) sources. I added supplementary features to the existing sources.
    * TCP feature addition for existing FREMM dissector allowing extraction and real-time visualization of specific attributes triggering asynchronous events.
    * Client/serve tools for TCP packet generation from FREMM UDP packets.
    * Technical environment : C, Python, C++, Linux (virtualized RedHat).

## Internships

### Image processing engineer
#### Tenon Hospital - APHP
##### March 2007 - August 2007

Internship on 3D prostatic tumors visualization on Matlab. In charge of the design and development of visualization tools for a medical research unit. As a support engineer, my responsibilites included :

* Designing a new 3D visualization model of prostatic tumor from prostatic biopsies. A proof of concept HMI has been developed for doctors, allowing interactive 3D visualization on Matlab.
* Prototyping a 3D prostatic tumor deformation model (OpenGL, C++).
* Speckle noise filter development for TRUS echographic images of the prostate (Matlab sources available on Mathworks).

### Web Designer
#### Washington Navy Museum
##### Summer 2002

* Revamp of the museum's website : rethinking how the exhibits are displayed on the website and design proposal according to the graphical guidelines of the department (HTML, CSS).
* Adding new exhibits to the website : photo and scan of the pertinent artefacts (Photoshop).

## Referees

Available on request.
