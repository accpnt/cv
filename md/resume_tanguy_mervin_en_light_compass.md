::: {.center}
# Tanguy **Mervin**

[tmervin0011@icloud.com](mailto:tmervin0011@icloud.com) • 0623062379 • Paris, France
:::

---

## Analyst

Dedicated and highly analytical economics professional with a Master's degree in economics, specializing in econometrics. Seeking a challenging role as an Economic Analyst at Compass Lexecon to leverage strong analytical and organizational skills in contributing to empirical analysis and providing valuable insights to clients. Recently graduated from EFAB and proficient with several data processing tools.  

## Skills

* Highly analytical: proven ability to analyze qualitative and quantitative client data
* Capable of working independently as well as collaboratively within a team to achieve project goals
* Strong ability to communicate complex economic concepts effectively with both colleagues and clients.
* Fluent in both French and English

## Education

* EFAB: Master's degree in Economics and data analysis (SEND) (2021 - 2022)
* CNAM: Bachelor of Honor in Quantitative analysis and risk statistics for finance (2018 - 2021)
* CNAM: Bachelor Applied mathematics and statistics (2018 - 2019)
* Ulm Universität, Germany: Master 2 Communications Technology (2006 - 2007)
* ENSSAT, France: Engineering Master's degree (2004 - 2007)


## Experience

### Data Analyst
#### Kaiko
##### December 2023 - present

* Provide quality assurance strategies for financial digital assets data pipelines and validation of quantitative models.

### Data Consultant
#### Freelance for Siemens Mobility
##### November 2021 - December 2023

* Data consultant for Siemens Mobility (June 2022 - November 2023)
    * Conduct analysis for NExTEO, ATS+, and PL14 projects.
    * Provide insights on railway signalization data through client-driven dashboards.
* IoT architect for Beerlink (April 2022 - July 2022)
* Software architect for New Imaging Technologies (January 2022 - April 2022)

### Data Scientist
#### Deeplime
##### January 2020 - August 2021

* Conducted 3D variography, kriging estimation, uniform conditioning modeling.
* Applied open pit calculus with graph theory and linear algebra.
* Validated algorithms through unit tests.

### Software engineer - Siemens Mobility
#### Siemens Mobility
##### April 2018 - January 2020

* Developed test simulators for public transportation in the Greater Paris area.
* Provided insights on simulation data for the NExTEO project.

### Technical support engineer
#### Freelance
##### June 2017 - December 2017

### Software Engineer
#### Elsys Design
##### August 2010 - February 2017

### Working holiday in New Zealand
##### August 2009 - May 2010

### Software Engineer
#### Orange Business Services
##### March 2008 - August 2009

