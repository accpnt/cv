# Tanguy Mervin

[tmervin0010@icloud.com](mailto:tmervin0010@icloud.com) • +33623062379 • [GitLab](https://gitlab.com/accpnt) • Paris, France

## Software Engineer

Experienced software engineer with a strong foundation in mathematics, data analysis, and a recent focus on mastering the Rust programming language for applications in finance, following graduation in quantitative analysis. Known for a problem-solving mindset and a passion for learning and applying new technologies. 

## Experience

::: {.spread}
### Data Consultant
#### November 2021 - Current
:::
* Data consultant for Siemens Mobility (june 2022 - current)
    * Data analysis and engineering for NExTEO, ATS+ and PL14 projects
    * Functional skills: providing insights on railway signalization data, through client-driven dashboards
    * Technical skills: Splunk, Grafana, Apache Kafka, Clickhouse SQL, Python, Linux
* IoT architect for Beerlink (april 2022 - july 2022)
* Software architect for New Imaging Technologies (january 2022 - april 2022)

::: {.spread}
### Data Scientist - Deeplime
#### January 2020 - August 2021
:::

* Developpment of a spatial statistics Python library, with algorithm validation through automated unit tests (Python, R, CI/CD)

::: {.spread}
### Software Engineer - Siemens Mobility
#### April 2018 - January 2020
:::

* Test simulators development for the Greater Paris area public railways (C/C++)
* Consultant for SNCF and RATP on the NExTEO project

::: {.spread}
### Technical support engineer - Freelance
#### June 2017 - December 2017
:::

---

::: {.spread}
### Software Engineer - Elsys Design
#### August 2010 - February 2017
:::

* Linux kernel drivers and embedded firmware development for Sagem’s thermic cameras

---

::: {.spread}
### Working holiday in New Zealand
#### August 2009 - May 2010
:::

---

::: {.spread}
### Software Engineer - Orange Business Services
#### March 2008 - August 2009
:::

* Design and development of an embedded access controller software for Mobile IP routers with high handover capabilities (WiFi, satellite, 3G).

---

## Education

* EFAB: Master 2 Economics and data analysis (SEND) (2021 - 2022)
* CNAM: Master 2 Risk statistics for finance and business (2019 - 2020)
* CNAM: Master 1 Applied mathematics and statistics (2018 - 2021)
* CNAM: Bachelor Applied mathematics and statistics (2018 - 2019)
* Ulm Universität, Germany: Master 2 Communications Technology (2006 - 2007)
* ENSSAT, France: Engineering Master's degree (2004 - 2007)

## Skills

* Technical: C, C++, Rust, SQL, Python, R, Linux
* Functional: data analysis and software engineering, algorithm development, visualization and modelling, applied mathematics and statistics
* Miscellaneous: curiosity, problem-solving, critical thinking

## Personal and open source projects

* [Time series smoothing library](https://gitlab.com/accpnt/smooth) in Rust
* Design of [SHA-1](https://gitlab.com/accpnt/sha1crack) and [DMX](https://gitlab.com/accpnt/dmx) cores
* A minimalist [static website generator](https://gitlab.com/accpnt/staticgen) written in Rust
