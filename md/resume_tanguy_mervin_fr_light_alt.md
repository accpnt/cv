# Tanguy Mervin

[tmervin0011@icloud.com](mailto:tmervin0011@icloud.com) • 0623062379 • Projets personnels sur [GitLab](https://gitlab.com/accpnt) • Paris, France

## Spécialiste Data

Quatre ans d'expérience dans la data pour la transformation de données brutes en information stratégique. Compétences avancées en traitement et visualisation de données, modélisation statistique et intégration d'infrastructure pour la gestion de données massives. Expert dans l'utilisation d'outils tels que SQL et Python pour la résolution de problèmes complexes. 

## Expérience

::: {.spread}
### QA Data Analyst - Kaiko
#### Décembre 2023 - Présent
:::

* Stratégies QA pour des pipelines de données financières et la validation de modèles quantitatifs
* Implémentation de pipelines de test pour la validation du calcul de la volatilité implicite et des indices (Python, Clickhouse, SQL, Rust)

::: {.spread}
### Spécialiste Data - Freelance pour Siemens Mobility
#### Novembre 2021 - Décembre 2023
:::
* Spécialiste Data pour Siemens Mobility (juin 2022 - décembre 2023)
    * Analyse de données volumineuses pour identifier les anomalies de fonctionnement et les axes d'amélioration de la circulation des trains CBTC pour le projet NExTEO/ATS+
    * Création de tableaux de bord interactifs pour la visualisation de la demande future en exploitation et maintenance des produits Trainguard CBTC (Splunk, Grafana)
    * Développement de scripts d'agrégation de données pour mettre en évidence les KPI clés (Python)
    * Collaboration étroite avec les équipes produit et système pour l'identification des défauts de fonctionnement et l'ajustement des stratégies de maintenance ferroviaire
    * Intégration d'un système de stockage de données massives (Apache Kafka, telegraf, Clickhouse SQL, Prometheus)
* Architecte IoT pour Beerlink (avril 2022 - juillet 2022)
* Architecte logiciel pour New Imaging Technologies (janvier 2022 - avril 2022)

::: {.spread}
### Data Scientist - Deeplime
#### Janvier 2020 - Août 2021
:::

* Collecte, nettoyage et traitement de données minières à partir de diverses sources (propriétaires et Open Data) pour alimenter des analyses statistiques approfondies sur l'exploitation minières.
* Modélisation et développement d'une librairie de calcul en statistiques spatiales pour l'estimation des ressources minières (Python)
* Validation des algorithmes et des modèles statistiques par tests unitaires automatisés (R, Python)

::: {.spread}
### Ingénieur systèmes de test - Siemens Mobility
#### Avril 2018 - Janvier 2020
:::

* Développement sur les simulateurs de test des métros automatisés CBTC (projet NExTEO)
* Analyse de données et traitement de logs de simulation ferroviaire (Splunk, Python)

::: {.spread}
### Ingénieur de support technique - Freelance
#### Juin 2017 - Décembre 2017
:::

---

::: {.spread}
### Ingénieur de développement - Elsys Design
#### Août 2010 - Février 2017
:::

---

::: {.spread}
### Working holiday en Nouvelle-Zélande
#### Août 2009 - Mai 2010
:::

---

::: {.spread}
### Ingénieur de développement - Orange Business Services
#### Mars 2008 - Août 2009
:::

---

## Formations

* EFAB: Master 2 Stratégies économiques, numérique et données (SEND) (2021 - 2022)
* CNAM: Master 2 Statistique du risque pour la finance et l’assurance (2019 - cursus supprimé en 2021)
* CNAM: Master 1 Mathématiques appliquées et statistiques (2018 - 2021)
* CNAM: Licence générale mention Mathématiques Appliquées (2018 - 2019)
* Ulm Universität: Master 2 Communications Technology (Septembre 2006 - Avril 2007)
* ENSSAT: Diplôme d'ingénieur en Electronique et Informatique Industrielle (2004 - 2007)

## Compétences techniques et fonctionnelles

* Langages: SQL, Python, R, C/C++, Rust
* Outils: Splunk, Grafana, Tableau, Apache Kafka, Confluent telegraf, Clickhouse SQL, Prometheus, Git, GitLab
* Statistiques: modélisation, tests hypothétiques, régression
* Manipulation de données: nettoyage, transformation, agrégation
* Stratégies économiques et business: analyse sectorielle, design économique, économie numérique
* Fonctionnel: analyse de données approfondie, communication efficace des résultats, résolution de problèmes complexes




