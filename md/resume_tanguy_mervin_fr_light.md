# Tanguy Mervin

[tmervin0010@icloud.com](mailto:tmervin0010@icloud.com) • 0623062379 • [GitLab](https://gitlab.com/accpnt) • [Blog](https://accpnt.eu/) • Paris, France

## Data Analyst / Scientist

Data Analyst avec trois années d'expérience dans différents secteurs (10+ années d'expérience totale). Expert en modélisation et manipulation de données avec des compétences avancées en Python et SQL. Expérience avérée dans la résolution de problèmes complexes, la communication et le reporting client. 

## Compétences

* Analyse de données: API, web scraping, SQL, Splunk, Tableau, Grafana, Kafka, Clickhouse
* Data science : traitement et visualisation de données, modélisation et calcul matriciel
* Informatique: Python, R, C/C++, Rust, Docker, Git, Github
* Stratégies économiques: analyse sectorielle, incitations et design économique, économie numérique

## Expériences

### Data Analyst
#### Freelance
##### Novembre 2021 - présent

* Data Analyst pour Siemens Mobility (juin 2022 - présent)
    * Extraction et recueil des données d'exploitation et de maintenance (Splunk, Clickhouse SQL)
    * Traitement et analyse de données ferroviaires pour l'ATS CBTC (Python, Kafka)
    * Mise en place des tableaux de bord pour le projet NExTEO (Grafana, Splunk)
* Architecte IoT pour Beerlink (avril 2022 - juillet 2022)
    * Interfaçage avec capteur de pression, prototypage électronique sur Arduino
* Architecte logiciel pour New Imaging Technologies (janvier 2022 - avril 2022)

### Data Scientist / Analyst
#### DeepLime
##### Janvier 2020 - Août 2021

* Traitement et analyse de données minières (Python)
* Développement d'une librairie de statistiques spatiales
    * Modélisation: variographie 3D, estimation par krigeage, conditionnement uniforme
    * Calcul du pourtour optimal minier par théorie des graphes, calcul matriciel (Python, R)
    * Validation des algorithmes et modèles par test unitaire (Python, R, CI/CD)
* Développement frontend sur un portail de calcul numérique et de visualisation 3D d'objets miniers (GIS)
* Préparation de supports à la formation en modélisation statistique

### Ingénieur systèmes de test
#### Siemens Mobility
##### Avril 2018 - Janvier 2020

* Développement réseau sur les simulateurs de test des métros automatisés CBTC.
* Data mining, analyse et traitement de logs de simulation ferroviaire (Splunk, Python)
* Reporting et gestion de projet client pour la SNCF

### Ingénieur de support technique
#### Freelance
##### Juin 2017 - Décembre 2017

---

### Ingénieur de développement
#### Elsys Design
##### Août 2010 - Février 2017

---

### Working holiday en Nouvelle-Zélande
##### Août 2009 - Mai 2010

---

### Ingénieur de développement
#### Orange Business Services
##### Mars 2008 - Août 2009

---

## Formations

* EFAB: Master 2 Stratégies économiques, numérique et données (SEND) (2021 - 2022)
* CNAM: Master 2 Statistique du risque pour la finance et l’assurance (2019 - cursus supprimé en 2021)
* CNAM: Master 1 Mathématiques appliquées et statistiques (2018 - 2021)
* CNAM: Licence générale mention Mathématiques Appliquées (2018 - 2019)
* Ulm Universität, Allemagne: Master 2 Communications Technology (Septembre 2006 - Avril 2007)
* ENSSAT, France: Diplôme d'ingénieur en Electronique et Informatique Industrielle (2004 - 2007)
