# Tanguy Mervin

[tmervin0010@icloud.com](mailto:tmervin0010@icloud.com) • 0623062379 • [GitLab](https://gitlab.com/accpnt) • [Blog](https://accpnt.eu/) • Paris

## Ingénieur en mathématiques appliquées

Ancien ingénieur systèmes embarqués, puis data scientist / statisticien. Je suis très à l'aise avec les outils informatiques et d'analyse de données. 

## Compétences

* Data science : traitement de données (R, Python), data visualisation (plotly, matplotlib)
* Analyse de données: interfaçage API, web scraping, agrégation et reporting, SQL, Splunk, Tableau
* Informatique: Python, R, C/C++, Rust
* Embarqué: C sur différentes cibles (MSP430, NIOS II), spécifications et architecture, interfaçage (I2C, UART, SPI), intégration FPGA sur cibles Altera
* Mathématiques appliquées: géostatistiques, théorie des graphes, modélisation et calcul matriciel
* Stratégies économiques: analyse sectorielle, incitations et design économique, économie numérique

## Expériences

### Data Analyst
#### Freelance
##### Novembre 2021 - présent

* Data Analyst pour Siemens Mobility (juin 2022 - présent)
    * Création de dashboard Splunk pour la visualisation et statistiques de KPI d'exploitation ferroviaire (HTML, CSS, SPL)
    * Traitement de données industrielles pour le projet NExTEO (Python)
    * Stockage et extraction sur base de données (SQL)
* Architecte IoT pour Beerlink (avril 2022 - juillet 2022)
    * Interfaçage avec capteur de pression, prototypage électronique sur Arduino
* Architecte logiciel NIOS sur Cyclone V pour New Imaging Technologies (janvier 2022 - avril 2022)

### Data Scientist 
#### DeepLime
##### Janvier 2020 - Août 2021

* Développement d'une librairie de statistiques spatiales
    * Modélisation: variographie 3D, estimation par krigeage, conditionnement uniforme
    * Calcul du pourtour optimal minier par théorie des graphes, calcul matriciel (Python, R)
    * Validation des algorithmes et modèles par test unitaire (Python, R, CI/CD)
* Développement frontend sur un portail de calcul numérique et de visualisation 3D d'objets miniers (GIS)
* Préparation de supports à la formation en modélisation statistique

### Ingénieur systèmes de test
#### Siemens Mobility
##### Avril 2018 - Janvier 2020

* Développement embarqué et réseau sur les simulateurs de test des métros automatisés CBTC (C/C++).
* Data mining, analyse et traitement de logs de simulation (Splunk, Python)

### Ingénieur de support technique
#### Freelance
##### Juin 2017 - Décembre 2017

---

### Ingénieur de développement
#### Elsys Design
##### Août 2010 - Février 2017

* Intégration FPGA sur cibles Altera pour Sagem
* Architecture et développement embarqué sur NIOS II, MSP430 (Linux, FreeRTOS) 

### Working holiday en Nouvelle-Zélande
##### Août 2009 - Mai 2010

---

### Ingénieur de développement
#### Orange Business Services
##### Mars 2008 - Août 2009

---

## Formations

* EFAB: Master 2 Stratégies économiques, numérique et données (SEND) (2021 - 2022)
* CNAM: Master 2 Statistique du risque pour la finance et l’assurance (2019 - cursus supprimé en 2021)
* CNAM: Master 1 Mathématiques appliquées et statistiques (2018 - 2021)
* CNAM: Licence générale mention Mathématiques Appliquées (2018 - 2019)
* Ulm Universität, Allemagne: Master 2 Communications Technology (Septembre 2006 - Avril 2007)
* ENSSAT, France: Diplôme d'ingénieur en Electronique et Informatique Industrielle (2004 - 2007)
