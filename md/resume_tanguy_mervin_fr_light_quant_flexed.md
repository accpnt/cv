::: {.container}

::: {.left_panel}
## Contact

![mail](https://raw.githubusercontent.com/FortAwesome/Font-Awesome/6.x/svgs/solid/envelope.svg)  [tmervin0012@icloud.com](mailto:tmervin0012@icloud.com)  
![phone](https://raw.githubusercontent.com/FortAwesome/Font-Awesome/6.x/svgs/solid/phone.svg) +33623062379  
![gitlab](https://raw.githubusercontent.com/FortAwesome/Font-Awesome/6.x/svgs/brands/gitlab.svg) [GitLab](https://gitlab.com/accpnt)  
![position](https://raw.githubusercontent.com/FortAwesome/Font-Awesome/6.x/svgs/solid/location-dot.svg) Paris, France  


## Compétences 

* Python, C, C++, Rust, SQL
* Analyse de données
* Modélisation statistique
* Mathématiques appliquées
* Analyse quantitative


## Formations

* **Master 2** en Stratégies économiques (EFAB, 2022)
* **Master 1** en Statistiques du risque pour la finance (CNAM, 2021)
* **Licence générale** mention Mathématiques Appliquées (CNAM, 2019)
* **Diplôme d'ingénieur** en Electronique et Informatique (ENSSAT, 2007)

## Langues

* Français: natif
* Anglais: courant
* Allemand: notions

:::

:::{.right_panel}

# Tanguy **Mervin**

## IT Quant

Statisticien spécialisé en stratégies économiques, avec une option en risque pour la finance. Quinze ans d'expérience dans la transformation de données brutes en information stratégique, comprenant une récente immersion dans la finance décentralisée. Compétent en informatique, avec une solide capacité à déployer des outils analytiques.

## Expérience

### Analyste de données Senior
#### SNCF Immobilier
##### Avril 2024 - présent

* Suivi et coordination des projets de développement data du pôle immobilier
* Analyse de données financières et immobilières

### Analyste de données blockchain
#### Kaiko
##### Décembre 2023 - Avril 2024

* Implémentation de pipelines de test pour la validation:
    * des flux de données issus de différents exchanges crypto
    * du calcul de la volatilité implicite des options
    * des indices financiers

### Analyste de données
#### Freelance pour Siemens Mobility
##### Juin 2021 - Décembre 2023

* Analyse de données volumineuses pour l'identification des anomalies de fonctionnement des trains CBTC du projet NExTEO/ATS+
* Intégration d'un système de gestion de données massives (Kafka, Clickhouse SQL)
* Développement de tableaux de bord d'indicateurs ferroviaires 

### Architecte logiciel
#### Freelance pour New Imaging Technologies
##### Janvier 2022 - Avril 2022

* Conception d'un logiciel embarqué pour caméra thermique en C sur FPGA

### Statisticien
#### Deeplime
##### Janvier 2020 - Août 2021

* Collecte, nettoyage et traitement de données d'exploitation minières 
* Analyses statistiques approfondies (krigeage, variographie)
* Modélisation statistique et développement d'une librairie de calcul (Python, R)
* Compétences transférables: calcul d'indicateurs, modélisation statistique

### Ingénieur systèmes de test
#### Siemens Mobility
##### Avril 2018 - Janvier 2020

* Développement de modèles ferroviaires en C++ pour le projet NExTEO
* Analyse de données et traitement de logs de simulation ferroviaire

### Ingénieur de support technique
#### Freelance
##### Juin 2017 - Décembre 2017

### Ingénieur de développement
#### Elsys Design
##### Août 2010 - Février 2017

* Consultant en développement de logiciel embarqué en C, C++ et Python

### Working holiday en Nouvelle-Zélande
##### Août 2009 - Mai 2010

### Ingénieur de développement
#### Orange Business Services
##### Mars 2008 - Août 2009

* Développement logiciel sur routeurs Linux pour le projet Connexion TGV

## Contributions opensource

* [a0qo](https://gitlab.com/accpnt/a0qo): scrapeur de données financières et calcul d'indicateurs en Rust et Python
* [smooth](https://gitlab.com/accpnt/smooth): librairie de lissage de séries temporelles en Rust
* [not](https://gitlab.com/accpnt/not/): demos OpenGL/SDL en C++

:::
:::