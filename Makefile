V = 

ifeq ($(strip $(V)),)
	E = @echo
	Q = @
else
	E = @\#
	Q =
endif
export E Q


# Source and object file directories
SRC_DIR := md
OBJ_DIR := output

# List of source files
SRC_FILES := $(wildcard $(SRC_DIR)/*.md)

# Generate a list of HTML and PDF files
HTML_FILES := $(patsubst $(SRC_DIR)/%.md,$(OBJ_DIR)/%.html,$(SRC_FILES))
PDF_FILES := $(patsubst $(SRC_DIR)/%.md,$(OBJ_DIR)/%.pdf,$(SRC_FILES))

all: clean html pdf

pdf: $(PDF_FILES)

html: $(HTML_FILES)

# Rule to generate PDF from HTML
$(OBJ_DIR)/%.pdf: $(OBJ_DIR)/%.html
	$(E) "  PDF    " $@
	$(Q) weasyprint --quiet $< $@

# Rule to generate HTML from Markdown with "_flexed" in the filename
$(OBJ_DIR)/%_flexed.html: $(SRC_DIR)/%_flexed.md
	$(E) "  HTML   " $@
	@mkdir -p $(@D)
	$(Q) pandoc $< --quiet --embed-resources --standalone -c css/kushnir_flexed.css --from markdown+fenced_divs -t html -o $@

# General rule to generate HTML from Markdown
$(OBJ_DIR)/%.html: $(SRC_DIR)/%.md
	$(E) "  HTML   " $@
	@mkdir -p $(@D)
	$(Q) pandoc $< --quiet --embed-resources --standalone -c css/kushnir.css --from markdown+fenced_divs -t html -o $@

clean:
	$(E) "  CLEAN  " $@
	$(Q) rm -rf $(OBJ_DIR)

.PHONY: all clean pdf html

