#!/bin/bash

# Output Markdown file
output_file="cv.md"

# Directory containing HTML and PDF files
output_dir="output"

# Initialize the Markdown file
echo "# cv" > "$output_file"
echo "" >> "$output_file"

# Iterate over HTML files in the "output" directory
for file in "$output_dir"/*.html; do
    if [ -f "$file" ]; then
        # Get the filename without extension
		filename=$(basename "$file")
		filename_without_extension="${filename%.html}"
        
        # Create a Markdown entry with a link to the HTML and PDF files
        echo "${filename_without_extension} [HTML](${filename_without_extension}.html) [PDF](${filename_without_extension}.pdf)" >> "$output_file"
		echo "" >> "$output_file"
    fi
done

